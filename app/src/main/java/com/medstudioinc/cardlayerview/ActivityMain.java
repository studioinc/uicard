package com.medstudioinc.cardlayerview;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class ActivityMain extends AppCompatActivity {

    RecyclerView rv;
    CardAdapter adapter;
    ArrayList<CardModel> cardModels = new ArrayList<>();
    String alpha = "#B5";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv = findViewById(R.id.rv);

        adapter = new CardAdapter();

        rv.setLayoutManager(new GridLayoutManager(this, 2));
        rv.setHasFixedSize(true);
        rv.setItemViewCacheSize(20);
        rv.setDrawingCacheEnabled(true);
        rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rv.setNestedScrollingEnabled(false);
        rv.postDelayed(new Runnable() {
            @Override
            public void run() {
                rv.setAdapter(adapter);
            }
        },500);

        cardModels.add(new CardModel(0, R.drawable.conding, "Coding", new String[]{"A5CC82", "00467F"}));
        cardModels.add(new CardModel(1, R.drawable.ideas, "Ideas", new String[]{"FF0099", "493240"}));
        cardModels.add(new CardModel(2, R.drawable.drinks, "Drink", new String[]{"f5af19", "f12711"}));
        cardModels.add(new CardModel(3, R.drawable.food, "Food", new String[]{"ACBB78", "799F0C"}));
        cardModels.add(new CardModel(4, R.drawable.health, "Health", new String[]{"ffd452", "544a7d"}));
        cardModels.add(new CardModel(5, R.drawable.lifestyle, "Lifestyle", new String[]{"F7BB97", "DD5E89"}));
        cardModels.add(new CardModel(6, R.drawable.sport, "Sport", new String[]{"35beb2", "136a8a"}));
        cardModels.add(new CardModel(7, R.drawable.yoga, "Yoga", new String[]{"12c2e9", "c471ed"}));
        cardModels.add(new CardModel(8, R.drawable.surgery, "Hopital", new String[]{"26ACF5", "1D92F3"}));
        cardModels.add(new CardModel(9, R.drawable.addiction, "Addiction", new String[]{"EF1019", "F23C1F"}));
        cardModels.add(new CardModel(10, R.drawable.nature, "Beauty", new String[]{"3BABE0", "125EA2"}));
        cardModels.add(new CardModel(11, R.drawable.office, "Office", new String[]{"FB9627", "F57025"}));

        adapter.addCards(cardModels);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, Menu.NONE, "Licenses").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0:
                new AlertDialog.Builder(this).setMessage("Image source: \n\n www.pixabay.com").setNegativeButton("Ok", (dialog, which)-> {
                        dialog.cancel();
                        dialog.dismiss();
                }).show();
                return true;
        }
        return false;
    }
}
