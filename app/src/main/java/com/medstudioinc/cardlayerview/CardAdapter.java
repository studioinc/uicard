package com.medstudioinc.cardlayerview;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.medstudioinc.cardlayout.UICard;
import com.medstudioinc.cardlayout.listener.OnCardViewClickListener;
import com.medstudioinc.cardlayout.listener.OnLongCardViewClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;




public class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<CardModel> cardModels = new ArrayList<>();
    String[][] arrayNormalColors;
    String[][] arrayPressedColors;
    String alpha = "B9";

    public CardAdapter() {
    }

    public void addCards(ArrayList<CardModel> cardModels){
        for (CardModel cardModel : cardModels){
            this.add(cardModel);
        }

        this.addColors();
    }

    public void add(CardModel cardModel){
        this.cardModels.add(cardModel);
        this.notifyItemInserted(this.cardModels.size() -1);
    }

    public void addColors(){
        HashMap<String, String> hashMap = new HashMap<>();
        arrayNormalColors = new String[getItemCount()][2];
        arrayPressedColors = new String[getItemCount()][2];
        for (int i = 0; i < getItemCount(); i++){
            for (int x = 0; x < arrayNormalColors[i].length; x++){
                arrayNormalColors[i][x] = (String) cardModels.get(i).getColors()[x];
                arrayPressedColors[i][x] = (String) cardModels.get(i).getColors()[x];
            }
            hashMap.put(arrayNormalColors[i][0], arrayNormalColors[i][1]);
        }

        Set set = hashMap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            Log.d("NormalColors", mentry.getKey() + " : "+ mentry.getValue());
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ItemHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items, viewGroup, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CardModel cardModel = this.cardModels.get(i);
        ItemHolder holder = (ItemHolder) viewHolder;
        holder.UICard.setTitle(cardModel.getTitle());
        holder.UICard.setTitleShadow(2, -1, -2, Color.DKGRAY);
        holder.UICard.setTitleSize(12);
        holder.UICard.setHasThumbnail(true);
        holder.UICard.setThumbnail(cardModel.getIcon());
        holder.UICard.setTitleColor(android.R.color.white);
        holder.UICard.setTitleTypeface("fonts/future.ttf");
        holder.UICard.setBackgroundGradientColors(this.alpha, "#"+cardModel.getColors()[0], "#"+cardModel.getColors()[1]);
        holder.UICard.setRoundedCorners(15);

    }


    private String first(String[][] array, int i){
        return array[i][0];
    }

    private String second(String[][] array, int i){
        return array[i][1];
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements OnCardViewClickListener, OnLongCardViewClickListener {
        UICard UICard;
        public ItemHolder(@NonNull final View itemView) {
            super(itemView);
            this.UICard = (UICard) itemView.findViewById(R.id.card_view);
            this.UICard.setOnCardViewClickListener(this);
            this.UICard.setOnCardViewLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(itemView.getContext(), "Short click", Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onLongClick(View v) {
            Toast.makeText(itemView.getContext(), "Long click and consumed", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    @Override
    public long getItemId(int position) {
        return cardModels.get(position).getId(); //id()
    }

    @Override
    public int getItemCount() {
        return this.cardModels.size();
    }
}
