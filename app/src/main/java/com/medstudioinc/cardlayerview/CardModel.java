package com.medstudioinc.cardlayerview;

public class CardModel {
    public int id;
    public int icon;
    public String title;
    public String[] stringColors;


    public CardModel() {
    }

    public CardModel(int index, String title) {
        this.id = index;
        this.title = title;
    }

    public CardModel(int index, int icon, String title) {
        this.id = index;
        this.icon = icon;
        this.title = title;
    }

    public CardModel(int index, String title, String[] colors) {
        this.id = index;
        this.title = title;
        this.stringColors = colors;
    }

    public CardModel(int index, int icon, String title, String[] colors) {
        this.id = index;
        this.icon = icon;
        this.title = title;
        this.stringColors = colors;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String[] getColors() {
        return stringColors;
    }

    public void setColors(String[] colors) {
        this.stringColors = colors;
    }
}
