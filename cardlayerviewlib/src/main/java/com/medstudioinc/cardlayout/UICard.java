package com.medstudioinc.cardlayout;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.medstudioinc.cardlayerviewlib.R;
import com.medstudioinc.cardlayout.listener.OnCardViewClickListener;
import com.medstudioinc.cardlayout.listener.OnLongCardViewClickListener;
import com.medstudioinc.cardlayout.utils.DisplayUtility;
import com.medstudioinc.cardlayout.widgets.RoundedCornerView;


public class UICard extends ConstraintLayout {
    public ImageView cardThumbnail;
    public ConstraintLayout cardAlpha;
    public ConstraintLayout cardAlphaShadow;
    public TextView cardTitle;

    private MaterialRippleLayout cardRippleLayout;
    public RoundedCornerView cardRoundedCornerView;

    private Drawable cardThumbnailRes;

    private boolean cardHasThumbnail;
    private String cardTitleText;
    private ColorStateList cardTitleColor;
    private float cardTitleSize;
    private Typeface cardTitleTypeface;
    private int cardBackgroundSolidColor;
    private String[] cardBackgroundGradientColors;


    private float cardRoundedCornersRadius;

    String alpha = "39";

    private OnLongCardViewClickListener longCardViewClickListener;
    private OnCardViewClickListener cardViewClickListener;

    public UICard(Context context) {
        super(context);
    }

    public UICard(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.defaultAttrs(context, attrs, 0);
        this.views();
    }

    public UICard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.defaultAttrs(context, attrs, defStyleAttr);
        this.views();
    }

    private void defaultAttrs(Context context, AttributeSet attrs, int defStyleAttr){
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CardLayerView, defStyleAttr, defStyleAttr);
        try {
            this.cardHasThumbnail = typedArray.getBoolean(R.styleable.CardLayerView_cardHasThumbnail, false);
            this.cardTitleText = typedArray.getString(R.styleable.CardLayerView_cardTitleText);
            this.cardTitleColor = ColorStateList.valueOf(typedArray.getColor(R.styleable.CardLayerView_cardTitleColor, 0));
            this.cardTitleSize = DisplayUtility.dp2px(context, typedArray.getResourceId(R.styleable.CardLayerView_cardTitleSize, 0));
            this.cardBackgroundSolidColor = typedArray.getColor(R.styleable.CardLayerView_cardBackgroundSolidColor,0);
            this.cardRoundedCornersRadius = typedArray.getDimensionPixelSize(R.styleable.CardLayerView_cardRoundedCornersRadius,0);
        } finally {
            typedArray.recycle();
        }
    }

    private void defaultValues(){
        this.cardRoundedCornersRadius = 0;

        this.setTitle(this.cardTitleText);
        this.setTitleSize(this.cardTitleSize);
        this.setTitleColor(this.cardTitleColor);
        //this.setTitleShadow(0, 0, 0, Color.DKGRAY);

        if (this.cardHasThumbnail){
            this.setThumbnail(R.drawable.office);
            this.cardThumbnail.setVisibility(VISIBLE);
        } else {
            this.cardThumbnail.setVisibility(GONE);
        }
        this.setBackgroundGradientColors(this.alpha, "#A5CC82", "#00467F");
        this.setRoundedCorners(this.cardRoundedCornersRadius);
    }

    private void views(){
        inflate(getContext(), R.layout.card_items, this);

        this.cardRoundedCornerView = (RoundedCornerView) findViewById(R.id.rounded_corner_view);
        this.cardRippleLayout = (MaterialRippleLayout) findViewById(R.id.card_ripple_layout);

        this.cardAlpha = (ConstraintLayout) findViewById(R.id.card_alpha);
        this.cardAlphaShadow = (ConstraintLayout) findViewById(R.id.card_alpha_shadow);

        this.cardTitle = (TextView) findViewById(R.id.card_title);
        this.cardThumbnail = (ImageView) findViewById(R.id.card_thumbnail);

        this.cardRoundedCornerView.setOnClickListener(v-> {
            if (cardViewClickListener != null) {
                cardViewClickListener.onClick(v);
            }
        });
        this.cardRoundedCornerView.setOnLongClickListener(v-> {
            if (longCardViewClickListener != null){
                longCardViewClickListener.onLongClick(v);
            }
            return false;
        });
        this.defaultValues();
    }

    public void setTitle(@StringRes int strRes){
        this.cardTitleText = getResources().getString(strRes);
        if (this.cardTitle != null && this.cardTitleText != null){
            this.cardTitle.setText(this.cardTitleText);
        }
    }

    public void setTitle(String strRes){
        this.cardTitleText = strRes;
        if (this.cardTitle != null && this.cardTitleText != null){
            this.cardTitle.setText(this.cardTitleText);
        }
    }

    public void setTitleColor(@ColorRes int colorRes){
        this.cardTitleColor = ColorStateList.valueOf(getResources().getColor(colorRes));
        if (this.cardTitle != null && !this.cardTitleColor.isStateful()){
            this.cardTitle.setTextColor(this.cardTitleColor);
        }
    }

    public void setTitleColor(ColorStateList colorRes){
        this.cardTitleColor = colorRes;
        if (this.cardTitle != null && !this.cardTitleColor.isStateful()){
            this.cardTitle.setTextColor(this.cardTitleColor);
        }
    }

    public void setTitleColor(String colorRes){
        this.cardTitleColor = ColorStateList.valueOf(Color.parseColor(colorRes));
        if (this.cardTitle != null && !this.cardTitleColor.isStateful()){
            this.cardTitle.setTextColor(this.cardTitleColor);
        }
    }

    public void setTitleSize(float textSize){
        this.cardTitleSize = textSize;
        if (this.cardTitle != null && this.cardTitleSize != 0){
            this.cardTitle.setTextSize(this.cardTitleSize);
        }
    }

    public void setTitleTypeface(Typeface typeface){
        this.cardTitleTypeface = typeface;
        if (this.cardTitle != null && this.cardTitleTypeface != null){
            this.cardTitle.setTypeface(this.cardTitleTypeface);
        }
    }

    public void setTitleTypeface(String typefacePath){
        this.cardTitleTypeface = Typeface.createFromAsset(getContext().getAssets(), typefacePath);
        if (this.cardTitle != null && this.cardTitleTypeface != null){
            this.cardTitle.setTypeface(this.cardTitleTypeface);
        }
    }

    public void setTitleShadow(float radius, float dx, float dy, int color){
        if (this.cardTitle != null){
            this.cardTitle.setShadowLayer(radius, dx, dy, color);
        }
    }

    public void setBackgroundSolidColor(@ColorInt int solidColorRes){
        this.cardBackgroundSolidColor = solidColorRes;
        if (this.cardAlpha != null && this.cardBackgroundSolidColor != 0){
            this.cardAlpha.setBackgroundColor(this.cardBackgroundSolidColor);
        }
    }

    public void setBackgroundSolidColor(String solidColorRes){
        this.cardBackgroundSolidColor = Color.parseColor(solidColorRes);
        if (this.cardAlpha != null && this.cardBackgroundSolidColor != 0){
            this.cardAlpha.setBackgroundColor(this.cardBackgroundSolidColor);
        }
    }

    public void setBackgroundGradientColors(String alpha, String... gradientColorRes){
        this.cardBackgroundGradientColors = gradientColorRes;
        if (this.cardAlpha != null && gradientColorRes.length == 2){
            GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] {
                    Color.parseColor(setAlpha(alpha, this.cardBackgroundGradientColors)[0]),
                    Color.parseColor(setAlpha(alpha, this.cardBackgroundGradientColors)[1])
            });
            this.cardAlpha.setBackgroundDrawable(gd);
        }
    }

    public void setThumbnail(@DrawableRes int drawable){
        this.cardThumbnailRes = getResources().getDrawable(drawable);
        if (this.cardThumbnail != null && this.cardThumbnailRes != null && cardHasThumbnail){
            this.loadThumbnail(this.cardThumbnailRes).into(this.cardThumbnail);
            this.cardThumbnail.setVisibility(VISIBLE);
        } else {
            this.cardThumbnail.setVisibility(GONE);
        }
    }

    public void setThumbnail(Drawable drawable){
        this.cardThumbnailRes = drawable;
        if (this.cardThumbnail != null && this.cardThumbnailRes != null && cardHasThumbnail){
            this.loadThumbnail(this.cardThumbnailRes).into(this.cardThumbnail);
            this.cardThumbnail.setVisibility(VISIBLE);
        } else {
            this.cardThumbnail.setVisibility(GONE);
        }
    }

    public void setRoundedCorners(float roundedCorners){
        this.cardRoundedCornersRadius = roundedCorners;
        if (this.cardRoundedCornerView != null && this.cardRippleLayout != null) {
            this.cardRoundedCornerView.setRoundedCorners(this.cardRoundedCornersRadius);
            this.cardRippleLayout.setRippleRoundedCorners((int) this.cardRoundedCornersRadius);
        }
    }

    public void setHasThumbnail(boolean hasThumbnail) {
        this.cardHasThumbnail = hasThumbnail;
    }

    public void setRippleDelayClick(boolean delayClick){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleDelayClick(delayClick);
        }
    }

    public void setRippleAlpha(Integer rippleAlpha){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleAlpha(rippleAlpha);
        }
    }

    public void setRippleBackground(int rippleBackground){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleBackground(rippleBackground);
        }
    }

    public void setRippleOverlay(boolean rippleOverlay){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleOverlay(rippleOverlay);
        }
    }

    public void setRippleHover(boolean rippleHover){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleHover(rippleHover);
        }
    }

    public void setRippleColor(int rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleColor(rippleColor);
        }
    }

    public void setDefaultRippleAlpha(int rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setDefaultRippleAlpha(rippleColor);
        }
    }

    public void setRippleDuration(int rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleDuration(rippleColor);
        }
    }

    public void setRippleRadius(float rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRadius(rippleColor);
        }
    }

    public void setRippleDiameter(int rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleDiameter(rippleColor);
        }
    }

    public void setRippleFadeDuration(int rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleFadeDuration(rippleColor);
        }
    }

    public void setRippleInAdapter(boolean rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRippleInAdapter(rippleColor);
        }
    }

    public void setRipplePersistent(boolean rippleColor){
        if (this.cardRippleLayout != null){
            this.cardRippleLayout.setRipplePersistent(rippleColor);
        }
    }

    public TextView getCardTitle() {
        return cardTitle;
    }

    public ColorStateList getTitleColor() {
        return this.cardTitleColor;
    }

    public float getTitleSize() {
        return this.cardTitleSize;
    }

    public int getBackgroundSolidColor(){
        return this.cardBackgroundSolidColor;
    }

    public String[] BackgroundGradientColors(){
        return this.cardBackgroundGradientColors;
    }

    public ImageView getCardThumbnail(){
        return this.cardThumbnail;
    }

    public float getRoundedCornersRadius() {
        return this.cardRoundedCornersRadius;
    }

    public boolean isHasThumbnail() {
        return this.cardHasThumbnail;
    }

    public Drawable getThumbnailRes() {
        return this.cardThumbnailRes;
    }

    public int getShadowColor() {
        return this.cardBackgroundSolidColor;
    }

    public View getView() {
        return this.cardRoundedCornerView;
    }

    public String getTitleText() {
        return this.cardTitleText;
    }

    private String[] setAlpha(String alpha, String... colors){
        String[] colorsAlpha = new String[2];
        String[] alphas = new String[2];
        if (colors.length > 0){
            if (colors[0].startsWith("#") & colors[1].startsWith("#")){
                colorsAlpha[0] = colors[0].replaceFirst("#", "");
                colorsAlpha[1] = colors[1].replaceFirst("#", "");
                alphas[0] = "#" + alphaHasThumbnail(alpha) + colorsAlpha[0];
                alphas[1] = "#" + alphaHasThumbnail(alpha) + colorsAlpha[1];
            }
        }
        return alphas;
    }

    private String alphaHasThumbnail(String alpha){
        if (cardHasThumbnail){
            return alpha;
        }
        return "";
    }

    /**
     * Provides type independent options to customize loads with Glide.
     */
    public RequestOptions requestOptions(){
        return new RequestOptions();
    }


    /**
     * A helper method equivalent to calling {@link #requestOptions()} and then {@link
     * RequestBuilder#load(Object)} with the given model.
     *
     * @return A new request builder for loading a {@link Drawable} using the given model.
     */

    private RequestBuilder<Drawable> loadThumbnail(@Nullable Object file){
        return Glide.with(getContext()).load(file).apply(requestOptions().centerCrop());
    }

    /**
     * Register a callback to be invoked when this view is clicked. If this view is not
     * clickable, it becomes clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setClickable(boolean)
     */
    public void setOnCardViewClickListener(OnCardViewClickListener listener){
        this.cardViewClickListener = listener;
    }

    /**
     * Register a callback to be invoked when this view is clicked and held. If this view is not
     * long clickable, it becomes long clickable.
     *
     * @param listener The callback that will run
     *
     * @see #setLongClickable(boolean)
     */
    public void setOnCardViewLongClickListener(OnLongCardViewClickListener listener){
        this.longCardViewClickListener = listener;
    }
}
