package com.medstudioinc.cardlayout.listener;

import android.view.View;

public interface OnCardViewClickListener {
    void onClick(View v);
}
