package com.medstudioinc.cardlayout.listener;

import android.view.View;

public interface OnLongCardViewClickListener {
    boolean onLongClick(View v);
}
