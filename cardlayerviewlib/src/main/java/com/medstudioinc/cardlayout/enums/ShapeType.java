package com.medstudioinc.cardlayout.enums;

public enum ShapeType {
    RECTANGLE,
    OVAL,
    LINE,
    RING
}