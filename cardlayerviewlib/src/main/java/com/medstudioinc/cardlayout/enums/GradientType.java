package com.medstudioinc.cardlayout.enums;

public enum GradientType {
    LINEAR_GRADIENT,
    RADIAL_GRADIENT,
    SWEEP_GRADIENT
}