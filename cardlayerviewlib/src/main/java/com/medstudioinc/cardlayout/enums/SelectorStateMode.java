package com.medstudioinc.cardlayout.enums;

public enum SelectorStateMode {
    NORMAL,
    PRESSED
}
