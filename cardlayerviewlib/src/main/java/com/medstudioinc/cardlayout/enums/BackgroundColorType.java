package com.medstudioinc.cardlayout.enums;

public enum BackgroundColorType {
    SOLID,
    GRADIENT
}
