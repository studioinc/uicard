package com.medstudioinc.cardlayout.utils;

public class Pairs {
    public String first;
    public String second;

    public static Pairs with(){
        return new Pairs();
    }

    public Pairs() {
    }

    public Pairs(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public Pairs setFirst(String first) {
        this.first = first;
        return this;
    }

    public String getSecond() {
        return second;
    }

    public Pairs setSecond(String second) {
        this.second = second;
        return this;
    }
}
