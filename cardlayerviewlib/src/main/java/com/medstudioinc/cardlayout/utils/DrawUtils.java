package com.medstudioinc.cardlayout.utils;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.view.View;

public class DrawUtils {

    public static RippleDrawable getBackgroundDrawable(int pressedColor, Drawable backgroundDrawable) {
        RippleDrawable rippleDrawable = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rippleDrawable = new RippleDrawable(getPressedState(pressedColor), backgroundDrawable, null);
        }
        return rippleDrawable;
    }

    public static ColorStateList getPressedState(int pressedColor) {
        return new ColorStateList(new int[][]{ new int[]{}},new int[]{pressedColor});
    }

    //StateListDrawable instead selector
    public static StateListDrawable getSelector(Drawable normalDrawable, Drawable pressDrawable) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{android.R.attr.state_enabled, android.R.attr.state_pressed}, pressDrawable);
        stateListDrawable.addState(new int[]{android.R.attr.state_enabled}, normalDrawable);

        // Set the default state
        stateListDrawable.addState(new int[]{}, normalDrawable);
        return stateListDrawable;
    }

    @SuppressWarnings("deprecation")
    public static void setBackground(View view, Drawable d) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(d);
        } else {
            view.setBackgroundDrawable(d);
        }
    }

    public static void postInvalidateOnAnimation(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.postInvalidateOnAnimation();
        } else {
            view.invalidate();
        }
    }

    public static boolean deviceHaveRippleSupport() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) { return false; }
        else { return true; }
    }
}
