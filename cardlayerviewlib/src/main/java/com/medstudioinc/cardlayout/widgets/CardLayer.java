package com.medstudioinc.cardlayout.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.medstudioinc.cardlayerviewlib.R;

public class CardLayer {
    private Context ctx;
    private Resources res;
    private View completeView;
    private LinearLayout layerCard;
    private RelativeLayout shadowView;
    private ImageView thumbnail;
    private TextView title;

    private String titleText;
    private int titleColor;
    private Drawable thumbnailRes;
    private int iconColor;


    public CardLayer(Context context, boolean hasIcon) {
        this.ctx = context;
        this.res = ctx.getResources();
        completeView = LayoutInflater.from(ctx).inflate(R.layout.card_item, null);
        layerCard = (LinearLayout) completeView.findViewById(R.id.layer_card);
        shadowView = (RelativeLayout) completeView.findViewById(R.id.shadow_view);
        thumbnail = (ImageView) completeView.findViewById(R.id.thumbnail);
        title = (TextView) completeView.findViewById(R.id.title);

    }


    private void init(){

    }

    public View getView(){
        return completeView;
    }


    public CardLayer setTitle(@StringRes int strRes){
        this.titleText = res.getString(strRes);
        if (title != null){
            this.title.setText(titleText);
        }
        return this;
    }

    public CardLayer setTitle(String strRes){
        this.titleText = strRes;
        if (title != null){
            this.title.setText(titleText);
        }
        return this;
    }

    public CardLayer setTitleColor(@ColorInt int colorRes){
        this.titleColor = colorRes;
        if (title != null){
            this.title.setTextColor(this.titleColor);
        }
        return this;
    }

    public CardLayer setTitleColor(String colorRes){
        this.titleColor = Color.parseColor(colorRes);
        if (title != null){
            this.title.setTextColor(this.titleColor);
        }
        return this;
    }

    public CardLayer setThumbnail(@DrawableRes int drawable){
        this.thumbnailRes = res.getDrawable(drawable);
        if (thumbnail != null){
            this.thumbnail.setBackgroundDrawable(this.thumbnailRes);
        }
        return this;
    }

    public CardLayer setThumbnail(Drawable drawable){
        this.thumbnailRes = drawable;
        if (thumbnail != null){
            this.thumbnail.setBackgroundDrawable(this.thumbnailRes);
        }
        return this;
    }

}
