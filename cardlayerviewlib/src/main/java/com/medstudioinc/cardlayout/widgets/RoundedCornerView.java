package com.medstudioinc.cardlayout.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.medstudioinc.cardlayerviewlib.R;


public class RoundedCornerView extends RelativeLayout {
    private static float roundedCornersRadius;
    private static final int DEFAULT_ROUNDED_CORNERS = 0;

    private Bitmap maskBitmap;
    private Paint paint, maskPaint;
    private float cornerRadius;
    private AttributeSet attrs;
    private int defStyle;

    public RoundedCornerView(Context context) {
        super(context);
    }

    public RoundedCornerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundedCornerLayout, 0, 0);
        try {
            roundedCornersRadius = typedArray.getFloat(R.styleable.RoundedCornerLayout_corner_radius, DEFAULT_ROUNDED_CORNERS);
        } finally {
            typedArray.recycle();
        }
        this.attrs = attrs;
        init(context, this.attrs, 0);
    }

    public RoundedCornerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.attrs = attrs;
        this.defStyle = defStyleAttr;

        init(context, this.attrs, this.defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        //cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, roundedCornersRadius, metrics);
        cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, roundedCornersRadius, metrics);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        setWillNotDraw(false);
    }

    @Override
    public void draw(Canvas canvas) {
        Bitmap offscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas offscreenCanvas = new Canvas(offscreenBitmap);

        super.draw(offscreenCanvas);

        if (maskBitmap == null) {
            maskBitmap = createMask(canvas.getWidth(), canvas.getHeight());
        }

        offscreenCanvas.drawBitmap(maskBitmap, 0f, 0f, maskPaint);
        canvas.drawBitmap(offscreenBitmap, 0f, 0f, paint);

        //getRootView().setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.airport));
    }

    private Bitmap createMask(int width, int height) {
        Bitmap mask = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
        Canvas canvas = new Canvas(mask);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);

        canvas.drawRect(0, 0, width, height, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawRoundRect(new RectF(0, 0, width, height), cornerRadius, cornerRadius, paint);


        return mask;
    }


    public void setRoundedCorners(float corners){
        this.roundedCornersRadius = corners;
        init(getContext(), this.attrs, this.defStyle);
    }

}
