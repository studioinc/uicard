package com.medstudioinc.cardlayout.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.medstudioinc.cardlayerviewlib.R;
import com.medstudioinc.cardlayout.enums.BackgroundColorType;
import com.medstudioinc.cardlayout.enums.GradientType;
import com.medstudioinc.cardlayout.enums.OrientationType;
import com.medstudioinc.cardlayout.enums.SelectorStateMode;
import com.medstudioinc.cardlayout.enums.ShapeType;
import com.medstudioinc.cardlayout.utils.DisplayUtility;
import com.medstudioinc.cardlayout.utils.DrawUtils;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class CardLayerView extends RelativeLayout implements View.OnTouchListener{

    private View completeView;
    private Drawable thumbnailRes;
    private String titleTxt;
    private int titleColor;
    private int shadowColor;


    private boolean hasThumbnail;
    public RelativeLayout layerCard;
    public RelativeLayout shadowView;
    private ImageView thumbnail;
    private TextView title;

    private Point lastTouchedPoint;

    private final static int REVEAL_DURATION = 400;
    private final static int HIDE_DURATION = 500;


    private int primaryColor;
    private int accentColor;


    private int backgroundColor;
    private int borderSolidColor;
    private int[] borderGradientColor;
    private int rippleColor;

    private int stokeColor;
    private int stokeWidth;
    private int[] normalColors;
    private int[] pressedColors;
    private float cornersRadius;
    private float gradientRadius;

    private int left;
    private int top;
    private int right;
    private int bottom;

    private GradientType gradientType;
    private ShapeType shapeType;
    private OrientationType orientationType;
    private BackgroundColorType backgroundColorType;
    private SelectorStateMode selectorStateMode;



    public CardLayerView(Context context) {
        super(context);
        defaultValues();
        init();
    }

    public CardLayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        defaultValues();
        init();
    }

    public CardLayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

        defaultAttrs(context, attrs, defStyleAttr);

        defaultValues();
        init();
    }

    private void defaultValues(){
        this.primaryColor = getResources().getColor(R.color.colorPrimary);
        this.accentColor = getResources().getColor(R.color.colorAccent);

        this.backgroundColor = Color.parseColor("#FFFFFF");
        this.borderSolidColor = Color.parseColor("#69d8d8d8");
        this.borderGradientColor = new int[] {Color.parseColor("#69d8d8d8"), Color.parseColor("#EAEAEA")};
        this.rippleColor = Color.parseColor("#cccccc");
        this.stokeColor = Color.parseColor("#cccccc");

        this.stokeWidth = DisplayUtility.dp2px(getContext(), 0);

        this.normalColors = new int[] {Color.RED, Color.BLACK, Color.BLUE};
        this.pressedColors = new int[] {Color.GRAY, Color.GREEN, Color.DKGRAY};

        this.cornersRadius = 10f;
        this.gradientRadius = 0f;

        this.gradientType = GradientType.LINEAR_GRADIENT;
        this.shapeType = ShapeType.RECTANGLE;
        this.orientationType = OrientationType.TOP_BOTTOM;
        this.backgroundColorType = BackgroundColorType.SOLID;
        this.selectorStateMode = SelectorStateMode.NORMAL;
    }


    private void defaultAttrs(Context context, AttributeSet attrs, int defStyleAttr){
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CardLayerView, defStyleAttr, 0);
        try {
            //hasThumbnail = typedArray.getBoolean(R.styleable.CardLayerView_hasThumbnail, false);
            //titleTxt = typedArray.getString(R.styleable.CardLayerView_hasThumbnail);
            //titleColor = typedArray.getColor(R.styleable.CardLayerView_hasThumbnail, 0);
            //shadowColor = typedArray.getColor(R.styleable.CardLayerView_hasThumbnail,0);
        } finally {
            typedArray.recycle();
        }
    }

    /*
     * Create TextView
     */
    private TextView textView(){
        TextView textView = new TextView(getContext());
        textView.setText("Textview");
        return textView;
    }

    /**
     * Create Button
     * @return
     */
    private Button button(){
        Button button = new Button(getContext());
        button.setText("Button");
        return button;
    }

    /**
     * Create ImageView
     * @return
     */
    private ImageView imageView(){
        ImageView imageView = new ImageView(getContext());
        imageView.setImageDrawable(thumbnailRes);
        return imageView;
    }

    /**
     * Create RelativeLayout
     * @return
     */
    private RelativeLayout relativeLayout() {
        RelativeLayout relativeLayout = new RelativeLayout(getContext());

        // SET THE SIZE
        relativeLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 100));

        // SET BACKGROUND COLOR JUST TO MAKE LAYOUT VISIBLE
        relativeLayout.setBackgroundColor(Color.GREEN);
        return relativeLayout;
    }


    /**
     * Create LinearLayout
     * @return
     */
    private LinearLayout linearLayout(){
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 100));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        return linearLayout;
    }


    private void init(){
        completeView = inflate(getContext(), R.layout.card_item, this);
        layerCard = (RelativeLayout) completeView.findViewById(R.id.layer_card);
        shadowView = (RelativeLayout) completeView.findViewById(R.id.shadow_view);
        thumbnail = (ImageView) completeView.findViewById(R.id.thumbnail);
        title = (TextView) completeView.findViewById(R.id.title);

        //shadowView.setVisibility(GONE);
    }

    public void setTitle(@StringRes int strRes){
        this.titleTxt = getResources().getString(strRes);
        if (title != null){
            title.setText(this.titleTxt);
        }
    }

    public void setTitle(String strRes){
        this.titleTxt = strRes;
        if (title != null){
            title.setText(this.titleTxt);
        }
    }

    public void setTitleColor(@ColorInt int colorRes){
        this.titleColor = colorRes;
        if (title != null){
            title.setTextColor(titleColor);
        }
    }

    public void setTitleColor(String colorRes){
        this.titleColor = Color.parseColor(colorRes);
        if (title != null){
            title.setTextColor(titleColor);
        }
    }

    public void setShadowColor(@ColorInt int colorRes){
        this.shadowColor = colorRes;
        if (shadowView != null){
            shadowView.setBackgroundColor(shadowColor);
        }
    }

    public void setShadowColor(String colorRes){
        this.shadowColor = Color.parseColor(colorRes);
        if (shadowView != null){
            shadowView.setBackgroundColor(shadowColor);
        }
    }

    public void setThumbnail(@DrawableRes int drawable){
        this.thumbnailRes = getResources().getDrawable(drawable);
        if (completeView != null){
            this.completeView.setBackgroundDrawable(this.thumbnailRes);
        }
    }

    public void setThumbnail(Drawable drawable){
        this.thumbnailRes = drawable;
        if (completeView != null){
            this.completeView.setBackgroundDrawable(this.thumbnailRes);
        }
    }


    public void apply(){
        // Set GradientDrawable as ImageView source image
        if (this.layerCard != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                RippleDrawable rippleDrawable = DrawUtils.getBackgroundDrawable(rippleColor, new RippleDrawable(DrawUtils.getPressedState(rippleColor), DrawUtils.getSelector(
                        getDrawable(SelectorStateMode.NORMAL),
                        getDrawable(SelectorStateMode.NORMAL)), null));

                this.layerCard.setBackground(rippleDrawable);
            } else {
                this.layerCard.setBackgroundDrawable(DrawUtils.getSelector(getDrawable(SelectorStateMode.NORMAL), getDrawable(SelectorStateMode.PRESSED)));
            }
        }
    }


    //GradientDrawable instead Drawable
    public Drawable getDrawable(SelectorStateMode selectorStateMode) {
        this.selectorStateMode = selectorStateMode;
        // Initialize a new GradientDrawable
        GradientDrawable background = new GradientDrawable();
        GradientDrawable border = new GradientDrawable();

        //Sets the radius of the gradient.
        background.setGradientRadius(gradientRadius);
        border.setGradientRadius(gradientRadius);

        //Sets the radius of the corner.
        background.setCornerRadius(cornersRadius);
        border.setCornerRadius(cornersRadius);

        background.setAlpha(50);
        border.setAlpha(50);


        // Set pixels width solid stoke color border
        background.setStroke(stokeWidth, stokeColor, 8, 5);//设置边框厚度和颜色
        border.setStroke(stokeWidth, stokeColor);//设置边框厚度和颜色


        // Set the color array to draw gradient
        setBackgroundColorType(new GradientDrawable[]{background, border});

        // Set GradientDrawable shape is a rectangle
        setShape(new GradientDrawable[]{background, border}, this.shapeType);

        // Set the GradientDrawable orientation
        setOrientation(new GradientDrawable[]{background, border}, this.orientationType);

        // Set the GradientDrawable gradient type linear gradient
        setGradientType(new GradientDrawable[]{background, border}, this.gradientType);


        Drawable[] layers = {background, border};
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layerDrawable.setPadding(16, 8, 16, 8);
        }

        layerDrawable.setLayerInset(0, 0, 0, 0, 0);
        layerDrawable.setLayerInset(1, left, top, right, bottom);





        /*
           Sets the center location of the gradient.
           setGradientCenter(float x, float y)
           //background.setGradientCenter(0,0);
        */

        // Set GradientDrawable width and in pixels
        //background.setSize(200, 200); // Width 400 pixels and height 100 pixels
        return layerDrawable;

    }

    private void setOrientation(GradientDrawable[] drawables, OrientationType orientationType){
        switch (orientationType){
            case TOP_BOTTOM:
                drawables[1].setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
                break;
            case TR_BL:
                drawables[1].setOrientation(GradientDrawable.Orientation.TR_BL);
                break;
            case RIGHT_LEFT:
                drawables[1].setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
                break;
            case BR_TL:
                drawables[1].setOrientation(GradientDrawable.Orientation.BR_TL);
                break;
            case BOTTOM_TOP:
                drawables[1].setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
                break;
            case LEFT_RIGHT:
                drawables[1].setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
                break;
            case BL_TR:
                drawables[1].setOrientation(GradientDrawable.Orientation.BL_TR);
                break;
        }
    }

    private void setGradientType(GradientDrawable[] drawables, GradientType gradientType){
        switch (gradientType){
            case LINEAR_GRADIENT:
                drawables[1].setGradientType(GradientDrawable.LINEAR_GRADIENT);
                break;
            case RADIAL_GRADIENT:
                drawables[1].setGradientType(GradientDrawable.RADIAL_GRADIENT);
                break;
            case SWEEP_GRADIENT:
                drawables[1].setGradientType(GradientDrawable.SWEEP_GRADIENT);
                break;
        }
    }


    private void setBackgroundColorType(GradientDrawable[] drawables){
        switch (this.backgroundColorType){
            case SOLID:
                //drawables[0].setColor(borderSolidColor);
                switch (selectorStateMode){
                    case NORMAL:
                        drawables[0].setColor(backgroundColor);
                        drawables[1].setColor(backgroundColor);

                        drawables[0].setAlpha(1);
                        drawables[1].setAlpha(1);
                        break;
                    case PRESSED:
                        drawables[0].setColor(rippleColor);
                        drawables[1].setColor(rippleColor);
                        break;
                }
                break;
            case GRADIENT:
                //drawables[0].setColors(borderGradientColor);
                switch (selectorStateMode){
                    case NORMAL:
                        drawables[0].setColors(normalColors);
                        drawables[1].setColors(normalColors);
                        break;
                    case PRESSED:
                        drawables[0].setColors(pressedColors);
                        drawables[1].setColors(pressedColors);
                        break;
                }
                break;
        }
    }

    private void setShape(GradientDrawable[] drawables, ShapeType shapeType){
        switch (shapeType){
            case RECTANGLE:
                drawables[0].setShape(GradientDrawable.RECTANGLE);
                break;
            case OVAL:
                drawables[0].setShape(GradientDrawable.OVAL);
                break;
            case LINE:
                drawables[0].setShape(GradientDrawable.LINE);
                break;
            case RING:
                drawables[0].setShape(GradientDrawable.RING);
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        lastTouchedPoint = new Point();
        lastTouchedPoint.x = (int) event.getX();
        lastTouchedPoint.y = (int) event.getY();

        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            if(!DrawUtils.deviceHaveRippleSupport()) {
                getRootView().setBackgroundColor(Color.argb(0x80, Color.red(accentColor), Color.green(accentColor), Color.blue(accentColor)));
            }

            // do nothing
            return true;
        }

        if(event.getAction() == MotionEvent.ACTION_CANCEL) {
            if(!DrawUtils.deviceHaveRippleSupport()) {
                getRootView().setBackgroundColor(primaryColor);
            }
            return true;
        }

        // new effects
        if(event.getAction() == MotionEvent.ACTION_UP) {

            if(!DrawUtils.deviceHaveRippleSupport()) {
                getRootView().setBackgroundColor(primaryColor);
            }
            else {
                // set the backgroundcolor
               /* this.background.reveal(lastTouchedPoint.x, lastTouchedPoint.y, Color.argb(0x80, Color.red(accentColor), Color.green(accentColor), Color.blue(accentColor)), 0, REVEAL_DURATION, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        background.reveal(lastTouchedPoint.x, lastTouchedPoint.y, primaryColor, 0, HIDE_DURATION, null);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });*/
            }
            return true;
        }

        return false;
    }





}
